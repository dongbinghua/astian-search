<?php
  if(isset($_GET['q'])) {
    require('search.php'); exit;
  } else {
?>
<?php require "misc/header.php"; ?>

    <title>AstianGO</title>
    </head>
    <body>
        <form class="search-container" action="/" method="get" autocomplete="off">
                <h1>Astian<span class="X">GO</span></h1>
                <input type="text" name="q" autofocus/>
                <input type="hidden" name="p" value="0"/>
                <input type="hidden" name="t" value="0"/>
                <input type="submit" class="hide"/>
                <div class="search-button-wrapper">
                    <button name="t" value="0" type="submit">Search with AstianGO</button>
                    <button name="t" value="3" type="submit">Search torrents with AstianGO</button>
                </div>
        </form>

<?php require "misc/footer.php"; ?>
<?php } ?>
