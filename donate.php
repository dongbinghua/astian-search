<?php
require "misc/header.php";

// Feel free to add your donation options here, but please don't remove mine.
?>

<title>AstianGO - Donate</title>
<body>
<div class="misc-container">
    
    <h1>Donate to the <a href="https://astian.org" target="_blank">Astian Team</a></h1>
    <h3><a href="https://buy.stripe.com/fZe17M8a18odck8bIQ">Donate with Stripe press here</a></h3>
    or copy and paste the link in you browser buy.stripe.com/fZe17M8a18odck8bIQ
    <h3>Bitcoin Cash (BCH):</h3>
    <p>1LpcACeYpYKD1A1mKKPt7TESRLXkzeANeh</p>
    <h3>Bitcoin (BTC):</h3>
    <p>1514oyoVDPLPbMmMUJ9aKBRR7PPts8tAA5</p>
    <h3>Litecoin (LTC):</h3>
    <p>LaAV1odiGWjmfc2rvJ7Thd2VJMTeFLyC6m</p>
    <h3>Binance Coin (BNB):</h3>
    <p>bnb1v6y2d8srnsm45654gdnpsex3e3lq5jgmc34vfk</p>
</div>

<?php require "misc/footer.php"; ?>
